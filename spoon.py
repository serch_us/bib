from __future__ import print_function, division, absolute_import, unicode_literals

import datetime
import json
from os import environ as env
import requests
import re
import StringIO
import time
from timeit import Timer
import sys
import logging as log

import dataset
from RISparser import readris
from RISparser.config import TAG_KEY_MAPPING

from ris_types import ris_types
import settings


def get_crossref_doi(freeform, attempts=0, max_attempts=2):
    # TODO: Exponential backoff
    if attempts >= max_attempts:
        return (-3, 'Too many attempts')
    #citation = json.dumps([freeform], ensure_ascii=False)
    citation = json.dumps([freeform], encoding='utf-8')
    r = requests.post('http://search.crossref.org/links', citation)
    r.headers['Content-Type'] = 'application/json'
    if r.ok and r.content:
        try:
            response = r.json()
            if response["query_ok"]:
                if len(response['results']) == 1:
                    if response['results'][0]['match']:
                        return (response['results'][0]['score'], response['results'][0]['doi'])
                    else:
                        return (-1, response['results'][0]['reason'])
                else:
                    return (-2, 'Multiple matches')
            else:
                raise ValueError("API response: query_ok is False: " + response["reason"])
        except Exception, e:
            raise e
    else:
        # Status code 502 is returned if the crossref search 
        # engine times out on an internal server. By using
        # a difference API call, the search engine will then
        # have a result in the cache that we can fetch.
        if r.status_code == 502:
            if attempts == 0:
                force_crossref_dois_search(freeform)
                time.sleep(5)
            attempts += 1
            get_crossref_doi(freeform, attempts)
        else:
            return (-4, 'HTTP Status: ' + str(r.status_code))

        if r.status_code == 500:
            raise ValueError("CrossRef API Internal Server Error")


def force_crossref_dois_search(query_str):
    # Crossref API call used to cache search results on remote
    # server so subsequent calls of other functions don't time out.
    try:
        url = u'http://search.crossref.org/dois?q={}&header=true'
        get = url.format(query_str.replace(' ', '+'))
        requests.get(get, timeout=15)
    except:
        pass

def look_for_existing_doi(freeform):
    freeform = re.sub(r"doi\s?:\s?", "doi:", freeform, flags=re.IGNORECASE)
    doi = re.search(r"(?:doi:|http://dx.doi.org/)10[.][0-9]{3,}(?:[.][0-9]+)*/\S+", freeform)
    if doi:
        return doi.group().replace("doi:", "http://dx.doi.org/")
    else:
        return None

def negotiate_content(doi, content_type):
    #http://www.crosscite.org/cn/
    content_types = {'bibtex'   : 'application/x-bibtex',
                     'ris'      : 'application/x-research-info-systems',
                     'apa'      : 'text/x-bibliography; style=apa',
                     'cse'      : 'text/x-bibliography; style=council-of-science-editors-author-date',
                     'citeproc' : 'application/vnd.citationstyles.csl+json',
                     }

    header = {'Accept' : content_types[content_type]}
    r = requests.get(doi, headers=header)
    if r.ok and r.content:
        content = r.content.decode('utf-8')
        if '<!DOCTYPE' in content:
            raise ValueError('Invalid content type')
        return content
    elif r.status_code == 404:
        raise ValueError('DOI Not Found')
    else:
        return ''

def convert_ris_date(date_string):
    if len(date_string) == len('YYYY/MM/DD'):
        date_format = '%Y/%m/%d'
    elif len(date_string) == len('YYYY/MM'):
        date_format = '%Y/%m'
    elif len(date_string) == len('YYYY'):
        date_format = '%Y'
    return datetime.datetime.strptime(date_string, date_format)

def parse_ris(ris_string):

    ris_string = fix_ris_er(ris_string)
    ris = ris_dict(ris_string)
    ris = fix_ris_fields(ris)
    return ris

def ris_dict(ris_string):

    # Add non-standard types from negotiated content to tag mapping.
    mapping = TAG_KEY_MAPPING
    mapping['SV'] = 'SV'
    return list(readris(StringIO.StringIO(ris_string.encode('utf-8')),mapping))[0]

def fix_ris_er(ris_string):
    # Fix RIS records that have a malformed end of record (ER) tag.
    fix_string = re.sub(r".*ER\s*-\s*$", 'ER  - \n', ris_string)
    return fix_string

def fix_ris_fields(ris_dict):
    if ris_dict.has_key('date'):
        ris_dict['date'] = convert_ris_date(ris_dict['date'])
    if ris_dict.has_key('type_of_reference'):
        ris_dict['type_of_reference'] = ris_types[ris_dict['type_of_reference']]
    if ris_dict.has_key('authors'):
        unicode_authors = []
        for author in ris_dict['authors']:
            unicode_authors.append(author.decode('utf-8'))
        ris_dict['authors'] = '; '.join(unicode_authors)
    return ris_dict

def time_call(func, *args, **kwargs):
    #http://stackoverflow.com/a/20975823   
    output_container = []
    def wrapper():
        output_container.append(func(*args, **kwargs))
    timer = Timer(wrapper)
    delta = timer.timeit(1)
    return delta, output_container.pop()

def spoon(freeform):

    record = {}
    result = look_for_existing_doi(freeform)
    if not result:
        score, result = get_crossref_doi(freeform)
    else:
        score = 99
    record.update(dict(score = score))
    if score > 0:
        try:
            record.update(dict(apa = negotiate_content(result, 'apa')))
            record.update(dict(ris = negotiate_content(result, 'ris')))
            record.update(parse_ris(record['ris']))
        except ValueError:
            record.update(dict(error = 'Content Negotiation Error'), score = -5)
    else:
        record.update(dict(error = result))
    return record


def scoop_out(id_value):
    tbl = env['FROM_TABLE']
    id_field = env['FROM_ID_FIELD']
    freeform_field = env['FROM_FREEFORM']
    url = env['FROM_URL']
    with dataset.connect(url) as db:
        source = db[tbl]
        filter = {}
        filter[id_field] = id_value
        record = source.find_one(**filter)
    #return record[freeform_field]
    return unicode(record[freeform_field], encoding='utf-8', errors='ignore')

def fill_bowl(scoop):
    tbl_name = env['TO_TABLE']
    with dataset.connect(engine_kwargs=(dict(connect_args = dict(charset='utf8')))) as db:
        table = db[tbl_name]
        table.insert(scoop)

def feed(container_info, id_value):
    settings.load(env, container_info)
    scoop = spoon(scoop_out(id_value))
    scoop[env['FROM_ID_FIELD']] = id_value
    fill_bowl(scoop)


def open_lid(container_info):
    settings.load(env, container_info)
    url = env['FROM_URL']
    tbl = env['FROM_TABLE']
    id_field = env['FROM_ID_FIELD']
    with dataset.connect(url) as db:
        source = db[tbl]
        records = source.all()
    for record in records:
        log.info('Spoon feed record {}'.format(record[id_field]))
        try:
            feed(container_info, record[id_field])
        except:
            log.debug("\tUnexpected error:", sys.exc_info()[0])


def test_record(id_value, config="TEST_SOURCE"):
    settings.load(env, config)
    free = scoop_out(id_value)
    print('Freeform:\n' + free + '\n')
    record = spoon(free)
    print('Record:\n' + str(record) + '\n')


def dev_copy(id_value, container_info):
    settings.load(env, container_info)
    freeform = scoop_out(id_value)
    settings.load(env, 'TEST_DEV')
    with dataset.connect(engine_kwargs=(dict(connect_args = dict(charset='utf8')))) as db:
        table = db[env['FROM_TABLE']]
        table.insert({env['FROM_FREEFORM']: freeform})

if __name__ == '__main__':

    # Must manually add 'DateAdded' column within MySQL Workbench:
    #ALTER TABLE `python_script`.`taccimo_source`
    #ADD COLUMN `DateAdded` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `secondary_title`;

    #settings.load(env, 'TEST_DEV')
    #open_lid('TEST_DEV')

    if len(sys.argv) == 1:
        while True:
                open_lid("TACCIMO_SOURCE")
                open_lid("TACCIMO_SUPPLIT")
                time.sleep(120)
    elif len(sys.argv) == 2:
        test_record(sys.argv[1])
    elif len(sys.argv) == 3:
        test_record(sys.argv[1], sys.argv[2])

