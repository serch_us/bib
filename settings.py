import os
import re
import inspect
from ConfigParser import SafeConfigParser

local_settings = {}

def _pathTo(fname):
    """Prepend the working directory to .env filename."""   
    if os.path.exists(fname):
        return fname    
    for frm in inspect.stack():
        mod = inspect.getmodule(frm[0])
        base_dir = os.path.dirname(mod.__file__)
        env_file = os.path.join(base_dir, fname)
        if os.path.exists(env_file):            
            return env_file
    if os.path.basename(fname) == fname:
        base_dir = os.path.dirname(__file__)
        return os.path.join(base_dir, fname)

def load(store=local_settings, section='DEFAULT', fname='.env'):
    """Read local default environment variables from a .env file 
    located in the project root directory. The .env file 
    should be placed in the .gitignore list.
    """    
    env_file = _pathTo(fname)
    #print "Using environment settings from: {}".format(env_file)
    _gitignore(env_file)
    _redact(env_file)
    
    with open(env_file) as f:        
        parser =  SafeConfigParser()        
        parser.optionxform = str     # Enables case-sensitivity for key name.
        parser.readfp(f)
    
    for key in [i[0] for i in parser.items(section)]:
        store.update({key : parser.get(section, key)})

def _redact(env_file):
    """Create a redacted copy of the .env file to be included
    in the project repository, ignoring commented lines.
    """    
    redacted = env_file + '.redacted'
    with open(env_file, 'r') as inf:
        with open(redacted, 'w') as outf:
            for line in inf.readlines():
                if line[0] != '#':
                    segments = re.split(r'[=:]', line)
                    if len(segments) > 1:
                        outf.write(segments[0].strip() + " = XXXXX\n")
                    else:
                        outf.write(line)
    
def _gitignore(env_file):
    """Create a .gitignore file if needed, or append .env to an existing one."""
    fname = os.path.basename(env_file)
    base_dir = os.path.dirname(env_file)
        
    gitignore = os.path.join(base_dir, '.gitignore')
    if not os.path.exists(gitignore):
        with open(gitignore, 'w') as envf:
            envf.write(fname)
    else:
        with open(gitignore, 'a+') as envf:
            ignored = False
            for line in envf.readlines():
                if line.find(fname) > -1:
                    ignored = True
            if not ignored:
                envf.write('\n# Environmental Variables\n' + fname)

load()
    
